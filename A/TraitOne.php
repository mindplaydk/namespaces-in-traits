<?php

namespace A;

use D\Component;

trait TraitOne
{
    public function methodOne(Component $c)
    {
        echo "methodOne invoked with: " . get_class($c) . "\n";
    }
}
