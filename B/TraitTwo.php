<?php

namespace B;

use E\Component;

trait TraitTwo
{
    public function methodTwo(Component $c)
    {
        echo "methodTwo invoked with: " . get_class($c) . "\n";
    }
}
