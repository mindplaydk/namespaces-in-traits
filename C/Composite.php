<?php

namespace C;

use A\TraitOne;
use B\TraitTwo;

class Composite
{
    use TraitOne;
    use TraitTwo;

    public function applyBoth(\D\Component $one, \E\Component $two)
    {
        $this->methodOne($one);
        $this->methodTwo($two);
    }
}
