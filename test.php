<?php

use C\Composite;

require __DIR__ . '/vendor/autoload.php';

$composite = new Composite();

$one = new \D\Component();

$composite->methodOne($one);

$two = new \E\Component();

$composite->methodTwo($two);

$composite->applyBoth($one, $two);
